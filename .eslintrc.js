module.exports = {
  env: {
    commonjs: true,
    es2021: true,
    jest: true,
    node: true,
  },
  extends: [
    'airbnb-base',
  ],
  parserOptions: {
    ecmaVersion: 12,
  },
  plugins: [
    'json-format',
  ],
  rules: {
  },
  settings: {
    'json/sort-package-json': false,
    'json/json-with-comments-files': ['.vscode/'],
  },
  overrides: [
    {
      files: ['*.js', '*.json'],
    },
  ],
};
