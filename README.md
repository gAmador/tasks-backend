# tasks-backend

Tasks CRUD application.

## Requirements

* [nodejs v14.15.3](https://nodejs.org/dist/v14.15.3/docs/api/)

## Usage

Start app

```bash
npm start
```
